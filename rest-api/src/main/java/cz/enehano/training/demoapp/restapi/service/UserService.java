package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserService {
  private final UserRepository userRepository;
  private final LoggedInUserService loggedInUserService;
  private final PasswordEncoder passwordEncoder;

  public UserService(UserRepository userRepository, LoggedInUserService loggedInUserService, PasswordEncoder passwordEncoder) {
    this.userRepository = userRepository;
    this.loggedInUserService = loggedInUserService;
    this.passwordEncoder = passwordEncoder;
  }

  public List<UserDto> getAllUserDtos() {
    return userRepository.findAllByOrderBySurnameAsc().stream()
        .map(UserDto::from)
        .collect(Collectors.toList());
  }

  public UserDto getUserDto(Long id) {
    return userRepository.findById(id)
        .map(UserDto::from)
        .orElseThrow(() -> userWithGivenIdDoesNotExistException(id));
  }

  @PreAuthorize("isAuthenticated()")
  public UserDto createUser(UserDto userDto) {
    Objects.requireNonNull(userDto);

    User user = new User();
    userRepository.save(mergeDtoToUser(userDto, user));

    return UserDto.from(user);
  }

  @PreAuthorize("isAuthenticated()")
  public UserDto updateUser(Long id, UserDto userDto) {
    Objects.requireNonNull(id);
    Objects.requireNonNull(userDto);

    User user = userRepository.findById(id)
        .orElseThrow(() -> userWithGivenIdDoesNotExistException(id));

    user = mergeDtoToUser(userDto, user);
    userRepository.save(user);

    return UserDto.from(user);
  }

  private static NoSuchElementException userWithGivenIdDoesNotExistException(Long id) {
    return new NoSuchElementException(String.format("User with id %d doesn't exist.", id));
  }

  @PreAuthorize("isAuthenticated()")
  public void deleteUser(Long id) {
    Objects.requireNonNull(id);

    if (userRepository.existsById(id)) {
      userRepository.deleteById(id);
    }
  }

  public User mergeDtoToUser(UserDto userDto, User user) {
    //TODO deal with code duplication and with "single" attribute updates
    user.setFirstName(userDto.getFirstName());
    user.setSurname(userDto.getSurname());
    user.setEmail(userDto.getEmail());
    user.setPhoneNumber(userDto.getPhoneNumber());
    user.setPassword(
        passwordEncoder.encode(userDto.getPassword())
    );
    if (user.getCreatorId() == null) {
      user.setCreatorId(loggedInUserService.getLoggedInUserId());
    }

    return user;
  }
}
