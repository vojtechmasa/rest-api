package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.security.EnehanoUserPrincipal;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SpringSecurityLoggedInUserService implements LoggedInUserService {
  @Override
  public Long getLoggedInUserId() {
    //TODO safely deal with NPE
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    if (!(principal instanceof EnehanoUserPrincipal)) {
      throw new AuthorizationServiceException("Principal is not of correct type.");
    }

    return ((EnehanoUserPrincipal) principal).getId();
  }
}
