package cz.enehano.training.demoapp.restapi.dto;

import cz.enehano.training.demoapp.restapi.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    public static UserDto from(User user) {
        Objects.requireNonNull(user);

        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setSurname(user.getSurname());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        String created = Optional.ofNullable(user.getCreated())
            .map(LocalDateTime::toString)
            .orElse(null);
        userDto.setCreated(created);
        userDto.setCreatorId(user.getCreatorId());

        return userDto;
    }

    private String firstName;
    private String surname;
    private String email;
    private String phoneNumber;
    private String password; // todo ignore on deserialization
    private String created;
    private Long creatorId;
}
