package cz.enehano.training.demoapp.restapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
    @SequenceGenerator(name = "user_generator", sequenceName = "user_seq", initialValue = 1000) //first 1000 ids used for testing
    private Long id;

    private String firstName;

    private String surname;

    @Email
    private String email;

    //TODO add validation
    private String phoneNumber;

    private String password;

    private LocalDateTime created;

    private Long creatorId;

    @PrePersist
    protected void onCreate() {
        created = LocalDateTime.now();
    }
}
