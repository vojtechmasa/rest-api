package cz.enehano.training.demoapp.restapi.service;

public interface LoggedInUserService {
  Long getLoggedInUserId();
}
