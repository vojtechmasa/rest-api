-- the hashed password is "secret"
INSERT INTO users (id, first_name, surname, email, phone_number, password, created, creator_id) VALUES
  (1, 'Peter', 'Pan', 'peter@pan.com', '123456789', '$2a$10$XjoFkPmXl03h7gzLRHgjb.sG65N0igWINEKxGo8h6R/0W/p6y5uYu', '2014-06-20 21:36:40', 1),
  (2, 'John', 'Doe', 'john@doe.com', '123456783', '$2a$10$XjoFkPmXl03h7gzLRHgjb.sG65N0igWINEKxGo8h6R/0W/p6y5uYu', '2014-06-20 21:36:40', 1);