package cz.enehano.training.demoapp.restapi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RestApiApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextLoads() {
	}

	@Test
	@Transactional
	public void getAllUsers_ok() throws Exception {
		mockMvc.perform(get("/users/all")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{\"firstName\":\"John\",\"surname\":\"Doe\",\"email\":\"john@doe.com\",\"phoneNumber\":\"123456783\",\"created\":\"2014-06-20T21:36:40\",\"creatorId\":1},{\"firstName\":\"Peter\",\"surname\":\"Pan\",\"email\":\"peter@pan.com\",\"phoneNumber\":\"123456789\",\"created\":\"2014-06-20T21:36:40\",\"creatorId\":1}]"));
	}

	@Test
	@Transactional
	public void getUserById_ok() throws Exception {
		mockMvc.perform(get("/users/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("{\"firstName\":\"Peter\",\"surname\":\"Pan\",\"email\":\"peter@pan.com\",\"phoneNumber\":\"123456789\",\"created\":\"2014-06-20T21:36:40\",\"creatorId\":1}"));
	}

	@Test
	@Transactional
	public void getUserById_nonExistentId_return404() throws Exception {
		mockMvc.perform(get("/users/9999")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	@WithUserDetails("peter@pan.com")
	public void createUser_ok() throws Exception {
		mockMvc.perform(post("/users")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"firstName\":\"David\",\"surname\":\"Lynch\",\"email\":\"david@lynch.com\",\"phoneNumber\":\"123456789\",\"password\":\"secret\"}"
				))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.firstName", is(equalTo("David"))))
				.andExpect(jsonPath("$.surname", is(equalTo("Lynch"))))
				.andExpect(jsonPath("$.phoneNumber", is(equalTo("123456789"))))
				.andExpect(jsonPath("$.creatorId", is(equalTo(1))))
				.andExpect(jsonPath("$.created", is(notNullValue())));
	}

	@Test
	@Transactional
	@WithAnonymousUser
	public void createUser_notAuthenticated_return401() throws Exception {
		mockMvc.perform(post("/users")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"firstName\":\"David\",\"surname\":\"Lynch\",\"email\":\"david@lynch.com\",\"phoneNumber\":\"123456789\",\"password\":\"secret\"}"
				))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	@Transactional
	@WithUserDetails("john@doe.com") //logged in user is different from original user creator
	public void updateUser_ok() throws Exception {
		mockMvc.perform(put("/users/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"firstName\":\"David\",\"surname\":\"Lynch\",\"email\":\"david@lynch.com\",\"phoneNumber\":\"123456789\",\"password\":\"secret\"}"
				))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.firstName", is(equalTo("David"))))
				.andExpect(jsonPath("$.surname", is(equalTo("Lynch"))))
				.andExpect(jsonPath("$.phoneNumber", is(equalTo("123456789"))))
				.andExpect(jsonPath("$.creatorId", is(equalTo(1))))
				.andExpect(jsonPath("$.created", is(notNullValue())));
	}

	@Test
	@Transactional
	@WithAnonymousUser
	public void updateUser_notAuthenticated_return401() throws Exception {
		mockMvc.perform(put("/users/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(
						"{\"firstName\":\"David\",\"surname\":\"Lynch\",\"email\":\"david@lynch.com\",\"phoneNumber\":\"123456789\",\"password\":\"secret\"}"
				))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

	@Test
	@Transactional
	@WithUserDetails("peter@pan.com")
	public void deleteUser_ok() throws Exception {
		mockMvc.perform(delete("/users/2")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk());

		mockMvc.perform(get("/users/all")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{\"firstName\":\"Peter\",\"surname\":\"Pan\",\"email\":\"peter@pan.com\",\"phoneNumber\":\"123456789\",\"password\":null,\"created\":\"2014-06-20T21:36:40\",\"creatorId\":1}]"));
	}

	@Test
	@Transactional
	@WithAnonymousUser
	public void deleteUser_notAuthenticated_return401() throws Exception {
		mockMvc.perform(delete("/users/2")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}

}
