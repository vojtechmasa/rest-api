package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import cz.enehano.training.demoapp.restapi.service.LoggedInUserService;
import cz.enehano.training.demoapp.restapi.service.UserService;
import org.assertj.core.util.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

// This test could be rewritten to UserServiceTest as UserController only delegates to UserService
@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
  private static final String FIRST_USER_FIRST_NAME = "John";
  private static final String FIRST_USER_SURNAME = "Doe";
  private static final String FIRST_USER_EMAIL = "john@doe.com";
  private static final String FIRST_USER_PHONE_NUMBER = "+420 222 333 444";
  private static final long FIRST_USER_CREATOR_ID = 1L;
  private final List<User> allUsers = Lists.newArrayList(
      new User(1L, FIRST_USER_FIRST_NAME, FIRST_USER_SURNAME, FIRST_USER_EMAIL, FIRST_USER_PHONE_NUMBER, "secret", LocalDateTime.now(), FIRST_USER_CREATOR_ID),
      new User(2L, "Peter", "Pan", "pan@pan.com", "+420 222 333 443", "secret2", LocalDateTime.now(), 1L),
      new User(3L, "Frida", "Khalo", "frida@ok.com", "+420 222 333 442", "secret3", LocalDateTime.now(), 1L)
  );

  @Mock
  private UserRepository mockedUserRepository;
  @Captor
  ArgumentCaptor<Long> longCaptor;

  private UserController userController;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    UserService userService = new UserService(mockedUserRepository, new StubLoggedInUserService(), new NoopPasswordEncoder());
    userController = new UserController(userService);
  }

  @After
  public void tearDown() {
  }

  @Test
  public void getAllUsers() {
    //GIVEN
    when(mockedUserRepository.findAllByOrderBySurnameAsc()).thenReturn(allUsers);

    //WHEN
    List<UserDto> actualUserDtos = userController.getAllUsers();

    //THEN
    UserDto[] expectedUserDtos = toUserDtos(allUsers);
    assertThat(actualUserDtos, containsInAnyOrder(expectedUserDtos));
  }

  private static UserDto[] toUserDtos(List<User> users) {
    return users.stream()
        .map(UserDto::from)
        .toArray(UserDto[]::new);
  }

  @Test
  public void getUser() {
    //GIVEN
    User user = allUsers.get(0);
    when(mockedUserRepository.findById(user.getId())).thenReturn(Optional.of(user));

    //WHEN
    UserDto actualUserDto = userController.getUser(user.getId());

    //THEN
    UserDto expectedUserDto = UserDto.from(user);
    assertThat(actualUserDto, is(equalTo(expectedUserDto)));
  }

  @Test
  public void createUser() {
    //GIVEN
    when(mockedUserRepository.save(any(User.class))).thenAnswer(returnsFirstArg());
    UserDto inputUserDto = UserDto.from(allUsers.get(0));
    inputUserDto.setPassword("secret");

    //WHEN
    UserDto actualOutputUserDto = userController.createUser(inputUserDto);

    //THEN
    assertThat(actualOutputUserDto.getFirstName(), is(equalTo(FIRST_USER_FIRST_NAME)));
    assertThat(actualOutputUserDto.getSurname(), is(equalTo(FIRST_USER_SURNAME)));
    assertThat(actualOutputUserDto.getPhoneNumber(), is(equalTo(FIRST_USER_PHONE_NUMBER)));
    assertThat(actualOutputUserDto.getPassword(), is(nullValue()));
    assertThat(actualOutputUserDto.getCreated(), is(nullValue())); //it's handled at DB layer
    assertThat(actualOutputUserDto.getCreatorId(), is(equalTo(FIRST_USER_CREATOR_ID))); //it's handled at DB layer
  }

  @Test
  public void updateUser() {
    //GIVEN
    User user = allUsers.get(0);

    when(mockedUserRepository.save(any(User.class))).thenAnswer(returnsFirstArg());
    Long userId = user.getId();
    when(mockedUserRepository.findById(userId)).thenReturn(Optional.of(user));

    UserDto originalInputUserDto = UserDto.from(user);
    originalInputUserDto.setPassword("secret");
    UserDto originalOutputUserDto = userController.createUser(originalInputUserDto);

    assertThat(originalOutputUserDto.getFirstName(), is(equalTo(FIRST_USER_FIRST_NAME)));
    assertThat(originalOutputUserDto.getSurname(), is(equalTo(FIRST_USER_SURNAME)));
    assertThat(originalOutputUserDto.getPhoneNumber(), is(equalTo(FIRST_USER_PHONE_NUMBER)));
    assertThat(originalOutputUserDto.getPassword(), is(nullValue()));
    assertThat(originalOutputUserDto.getCreated(), is(nullValue())); //it's handled at DB layer
    assertThat(originalOutputUserDto.getCreatorId(), is(equalTo(FIRST_USER_CREATOR_ID))); //it's handled at DB layer

    //WHEN
    String expectedFirstName = "Salvador";
    String expectedSurname = "Dali";
    UserDto updatedInputUserDto = new UserDto(expectedFirstName, expectedSurname, "a@b.cz", "123213", "password", null, 1L);
    UserDto actualUpdatedOutputUserDto = userController.updateUser(userId, updatedInputUserDto);

    //THEN
    assertThat(actualUpdatedOutputUserDto.getFirstName(), is(equalTo(expectedFirstName)));
    assertThat(actualUpdatedOutputUserDto.getSurname(), is(equalTo(expectedSurname)));
    assertThat(userId, is(equalTo(userId)));
    assertThat(user.getFirstName(), is(equalTo(expectedFirstName)));
    assertThat(user.getSurname(), is(equalTo(expectedSurname)));
  }

  @Test
  public void deleteUser() {
    //GIVEN
    Long userId = 1L;
    when(mockedUserRepository.existsById(userId)).thenReturn(true);

    //WHEN
    userController.deleteUser(userId);

    //THEN
    verify(mockedUserRepository).deleteById(longCaptor.capture());
    assertThat(longCaptor.getValue(), is(equalTo(userId)));
  }

  //TODO add negative scenarios testing

  private static final class NoopPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
      return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
      return Objects.equals(rawPassword, encodedPassword);
    }
  }

  private static final class StubLoggedInUserService implements LoggedInUserService {

    @Override
    public Long getLoggedInUserId() {
      return 1L;
    }
  }
}